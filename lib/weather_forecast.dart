import 'package:flutter/material.dart';

void main() {
  runApp(const WeatherForecastApp());
}

class WeatherForecastApp extends StatelessWidget {
  const WeatherForecastApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(brightness: Brightness.dark),
      home: Scaffold(
        body: buildBodyWidget(),
      ),
    );
  }
}

Widget buildBarWeather() {
  return ListTile(
      leading: IconButton(onPressed: () {}, icon: Icon(Icons.add)),
      title: Center(
        child: Text(
          "เมืองชลบุรี",
          style: TextStyle(fontSize: 20),
        ),
      ),
      subtitle: Row(
        children: <Widget>[
          Icon(
            Icons.near_me,
            size: 10,
          ),
          Icon(
            Icons.lens,
            size: 8,
            color: Colors.grey,
          ),
          Icon(
            Icons.lens,
            size: 8,
            color: Colors.grey,
          ),
          Icon(
            Icons.lens,
            size: 8,
            color: Colors.grey,
          )
        ],
        mainAxisAlignment: MainAxisAlignment.center,
      ),
      trailing:
      IconButton(onPressed: () {}, icon: Icon(Icons.settings_outlined)));
}

Widget buildCelsiusReport() {
  return ListTile(
    title: Row(
      children: [
        Text(
          "  31°",
          style: TextStyle(fontSize: 100),
        ),
      ],
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.end,
    ),
    subtitle: Row(
      children: [Text("แจ่มใส", style: TextStyle(fontSize: 20))],
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
    ),
  );
}

Widget aqiButton() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      TextButton(
        onPressed: () {},
        child: Row(
          children: [
            Icon(
              Icons.cloud,
              color: Colors.white,
            ),
            Text(
              "  AQI 44",
              style: TextStyle(color: Colors.white),
            )
          ],
        ),
        style: TextButton.styleFrom(
            backgroundColor: Colors.white.withOpacity(0.3), shape: StadiumBorder()),
      )
    ],
  );
}

Widget moreButton() {
  return Row(
    children: [
      TextButton(
          onPressed: () {},
          child: Row(
            children: [
              Text("รายละเอียดเพิ่มเติม ",
                  style: TextStyle(color: Colors.white)),
              Icon(
                Icons.arrow_forward_ios,
                color: Colors.white,
                size: 15,
              )
            ],
          )),
    ],
    mainAxisAlignment: MainAxisAlignment.end,
  );
}

Widget weatherPredict() {
  return Column(
    children: <Widget>[
      ListTile(
        leading: Icon(
          Icons.circle,
          color: Colors.yellowAccent,
        ),
        title: Text("วันนี้:แจ่มใส"),
        trailing: Text("31°/21°"),
      ),
      ListTile(
        leading: Icon(
          Icons.circle,
          color: Colors.yellowAccent,
        ),
        title: Text("พรุ่งนี้:แจ่มใส"),
        trailing: Text("29°/20°"),
      ),
      ListTile(
        leading: Icon(
          Icons.cloud,
          color: Colors.white,
        ),
        title: Text("อา.:มีเมฆ"),
        trailing: Text("29°/20°"),
      ),
    ],
  );
}

Widget predictButton() {
  return Row(
    children: <Widget>[
      TextButton(
        onPressed: () {},
        child: Text(
          "พยากรณ์ 5 วัน",
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
        style: TextButton.styleFrom(
          backgroundColor: Colors.white.withOpacity(0.3),
          shape: StadiumBorder(),
          padding: EdgeInsets.only(
              top: 20, bottom: 20, left: 100, right: 100),
        ),
      )
    ],
    crossAxisAlignment: CrossAxisAlignment.end,
    mainAxisAlignment: MainAxisAlignment.center,
  );
}

Widget buildBodyWidget() {
  return Container(
    color: Colors.blueAccent.shade100,
    child: ListView(
      children: <Widget>[
        Container(
          child: buildBarWeather(),
        ),
        Container(
          margin: EdgeInsets.only(top: 80),
          child: buildCelsiusReport(),
        ),
        Container(
          child: aqiButton(),
        ),
        Container(
          margin: EdgeInsets.only(top: 80),
          child: moreButton(),
        ),
        Container(
          child: weatherPredict(),
        ),
        Container(
          margin: EdgeInsets.only(top: 20),
          child: predictButton(),
        ),
      ],
    ),
  );
}
